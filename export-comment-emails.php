<?php
/**
 * Plugin Name: Export Comment Emails
 * Description: Export information of users, who have left comments on the blog.
 * Version: 1.0
 * Plugin URI: http://gretathemes.com
 * Author: GretaThemes
 * Author URI: http://gretathemes.com
 * Text Domain: export-comment-emails
 * Domain Path: /languages/
 * License: GPL v2 or later
 *
 * @package Export Comment Emails
 */

defined( 'ABSPATH' ) || die;

define( 'EXPORT_COMMENT_EMAILS_URL', plugin_dir_url( __FILE__ ) );

require plugin_dir_path( __FILE__ ) . 'inc/class-export-comment-emails-admin-page.php';
$admin_page = new Export_Comment_Emails_Admin_Page;
$admin_page->init();
