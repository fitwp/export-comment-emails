<?php
/**
 * Create the admin page for the plugin.
 *
 * @package Export Comment Emails
 */

/**
 * Admin page class.
 */
class Export_Comment_Emails_Admin_Page {
	/**
	 * All fields.
	 *
	 * @var array
	 */
	public $fields;

	/**
	 * List of filed choose for export.
	 *
	 * @var array
	 */
	public $export_fields;

	/**
	 * Initialize.
	 */
	public function init() {
		add_action( 'admin_menu', array( $this, 'add_admin_page' ) );

		$this->fields = apply_filters( 'export_comment_fields', array(
			'comment_author'       => esc_html__( 'Author Name', 'export-comment-emails' ),
			'comment_author_email' => esc_html__( 'Author Email', 'export-comment-emails' ),
			'comment_author_url'   => esc_html__( 'Author URL', 'export-comment-emails' ),
			'comment_author_IP'    => esc_html__( 'Author IP', 'export-comment-emails' ),
			'comment_date'         => esc_html__( 'Date', 'export-comment-emails' ),
			'comment_approved'     => esc_html__( 'Status', 'export-comment-emails' ),
		) );
	}

	/**
	 * Register the plugin page
	 */
	public function add_admin_page() {
		$page = add_submenu_page( 'tools.php', esc_html__( 'Export Comment Emails', 'export-comment-emails' ), esc_html__( 'Export Comment Emails', 'export-comment-emails' ), 'moderate_comments', 'export-comment-emails', array( $this, 'show' ) );
		add_action( "load-$page", array( $this, 'export' ) );
		add_action( "admin_print_styles-$page", array( $this, 'enqueue' ) );
	}

	/**
	 * Enqueue scripts and styles.
	 */
	public function enqueue() {
		wp_enqueue_style( 'jquery-ui-css', EXPORT_COMMENT_EMAILS_URL . 'css/jquery-ui.css' );
		wp_enqueue_script( 'export-comment-emails', EXPORT_COMMENT_EMAILS_URL . 'js/script.js', array( 'jquery-ui-datepicker' ), '1.0', true );
	}

	/**
	 * Display the plugin settings options page
	 */
	public function show() {
		?>
		<div class="wrap">
			<h1><?php esc_html_e( 'Export Comment Emails', 'export-comment-emails' ); ?></h1>
			<form method="post" action="">
				<?php wp_nonce_field( 'export' ); ?>
				<table class="form-table">
					<tbody>
					<tr>
						<th scope="row"><?php esc_html_e( 'Fields', 'export-comment-emails' ); ?></th>
						<td>
							<?php
							foreach ( $this->fields as $key => $label ) {
								printf( '<input type="checkbox" class="checkbox" id="fields[%1$s]" name="fields[%1$s]" value="%1$s" checked="checked">
									<label for="fields[%1$s]">%2$s</label><br>',
									esc_html( $key ),
									esc_html( $label )
								);
							}
							?>
						</td>
					</tr>
					<tr>
						<th scope="row"><?php esc_html_e( 'Format', 'export-comment-emails' ); ?></th>
						<td>
							<label><input type="radio" name="format" value="csv" checked><?php esc_html_e( 'CSV', 'export-comment-emails' ); ?> </label><br>
							<label><input type="radio" name="format" value="excel"><?php esc_html_e( 'Excel', 'export-comment-emails' ); ?></label>
						</td>
					</tr>
					<tr>
						<th scope="row"><?php esc_html_e( 'Date', 'export-comment-emails' ); ?></th>
						<td>
							<p>
								<select name="date" id="date">
									<option value="all"><?php esc_html_e( 'All', 'export-comment-emails' ); ?></option>
									<option value="custom"><?php esc_html_e( 'Custom', 'export-comment-emails' ); ?></option>
								</select>
							</p>
							<p class="hidden" id="custom-date">
								<label for="from-date"><?php esc_html_e( 'Form', 'export-comment-emails' ); ?></label>
								<input type="text" name="from_date" id="from-date">
								<label for="to-date"><?php esc_html_e( 'to', 'export-comment-emails' ); ?></label>
								<input type="text" name="to_date" id="to-date">
							</p>
						</td>
					</tr>
					</tbody>
				</table>
				<?php submit_button( __( 'Export', 'export-comment-emails' ) ); ?>
			</form>
		</div>
		<?php
	}

	/**
	 * Export comment emails.
	 */
	public function export() {
		$this->export_fields = isset( $_POST['fields'] ) ? $_POST['fields'] : array();

		if ( empty( $this->export_fields ) ) {
			return;
		}

		check_admin_referer( 'export' );

		$comments = get_comments( $this->query_args() );

		if ( empty( $comments ) ) {
			return;
		}

		$this->generate_file( $comments );
	}

	/**
	 * Generate .xlsx or .csv file using PHP_XLSXWriter class.
	 *
	 * @link https://github.com/mk-j/PHP_XLSXWriter
	 *
	 * @param array $comments list of comments.
	 */
	private function generate_file( $comments ) {
		if ( ! class_exists( 'PHPExcel' ) ) {
			require_once plugin_dir_path( __FILE__ ) . '/PHPExcel.php';
		}

		$php_excel = new PHPExcel();
		$columns   = range( 'B', 'Z' );

		// First column is No.
		$php_excel->setActiveSheetIndex()->setCellValue( 'A1', __( 'No', 'export-comment-emails' ) );
		$col = 0;

		// Table header.
		foreach ( $this->export_fields as $field ) {
			$php_excel->setActiveSheetIndex()->setCellValue( $columns[ $col ] . '1', $this->fields[ $field ] );
			$col ++;
		}

		// Data.
		$row = 1;
		foreach ( $comments as $comment ) {
			$row ++;
			$php_excel->setActiveSheetIndex()->setCellValue( 'A' . $row, $row - 1 );

			$col = 0;
			foreach ( $this->export_fields as $field ) {
				$php_excel->setActiveSheetIndex()->setCellValue( $columns[ $col ] . $row, $this->get_comment_field( $comment, $field ) );
				$col ++;
			}
		}

		$format      = filter_input( INPUT_POST, 'format' );
		$format      = $format ? $format : 'csv';
		$writer_type = 'excel' === $format ? 'Excel2007' : 'CSV';
		$extension   = 'excel' === $format ? '.xlsx' : '.csv';

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet.
		$php_excel->setActiveSheetIndex( 0 );

		$objWriter = PHPExcel_IOFactory::createWriter( $php_excel, $writer_type );
		$file = 'comment-emails-' . date( 'Y-m-d g:i' ) . $extension;

		// Redirect output to a client’s web browser (Excel2007).
		header( 'Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' );
		header( 'Content-Disposition: attachment;filename="' . $file . '"' );
		header( 'Cache-Control: max-age=0' );

		// If you're serving to IE 9, then the following may be needed.
		header( 'Cache-Control: max-age=1' );
		header( 'Expires: Mon, 26 Jul 1997 05:00:00 GMT' ); // Date in the past
		header( 'Last-Modified: ' . gmdate( 'D, d M Y H:i:s' ) . ' GMT' ); // always modified
		header( 'Cache-Control: cache, must-revalidate' ); // HTTP/1.1
		header( 'Pragma: public' ); // HTTP/1.
		$objWriter->save( 'php://output' );
		exit;
	}

	/**
	 * Get value field by column
	 *
	 * @param array $comment List of comments.
	 * @param array $field   Fields with comments (wp_comments) will be exported.
	 *
	 * @return string
	 */
	private function get_comment_field( $comment, $field ) {
		$value = isset( $comment->$field ) ? $comment->$field : '';

		if ( 'comment_approved' == $field ) {
			if ( '0' == $value ) {
				$value = __( 'Pending', 'export-comment-emails' );
			} elseif ( '1' === $value ) {
				$value = __( 'Approved', 'export-comment-emails' );
			}
		}

		return $value;
	}

	/**
	 * Get comment query args.
	 *
	 * @return array
	 */
	private function query_args() {
		$args = array();

		$date      = filter_input( INPUT_POST, 'date', FILTER_SANITIZE_STRING );
		$from_date = filter_input( INPUT_POST, 'from_date', FILTER_SANITIZE_STRING );
		$to_date   = filter_input( INPUT_POST, 'to_date', FILTER_SANITIZE_STRING );

		if ( 'all' === $date || ( ! $from_date && ! $to_date ) ) {
			return $args;
		}

		$args['date_query'] = array(
			'inclusive' => true,
		);

		if ( $from_date ) {
			$from_date = strtotime( $from_date );
			$args['date_query']['after'] = array(
				'year'  => date( 'Y', $from_date ),
				'month' => date( 'm', $from_date ),
				'day'   => date( 'd', $from_date ),
			);
		}
		if ( $to_date ) {
			$to_date = strtotime( $to_date );
			$args['date_query']['before'] = array(
				'year'  => date( 'Y', $to_date ),
				'month' => date( 'm', $to_date ),
				'day'   => date( 'd', $to_date ),
			);
		}

		return $args;
	}
}
