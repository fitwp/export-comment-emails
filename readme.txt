=== Export Comment Emails ===
Contributors: gretathemes, rilwis, manhdoan1392
Donate link: http://paypal.me/anhtnt
Tags: export comments, export comment emails, comment emails, export emails
Requires at least: 4.5
Tested up to: 4.7.3
Stable tag: 1.0
License: GPLv2 or later

Export comment emails to CSV or Excel (XLSX) file with date range filter.

== Description ==

When a visitor leave a comment on your website, it's a good chance for you to keep in touch with them via email. And you will need a tool to export all emails from your comments. Then you might import those emails into an email newsletter tool like MailChimp, ConvertKit, etc. **Export comment emails** allows you to that easily.

### Plugin Features

- Ability to chose which fields you want to export (author name, email, status, etc.).
- Supports CSV or Excel (XLSX) file formats.
- Supports export all comments or within a date range.

**Export Comment Emails** is made by [GretaThemes](https://gretathemes.com), a premium WordPress theme shop. If you like **Export Comment Emails**, you might also like our [WordPress themes](https://gretathemes.com/wordpress-themes/).

== Installation ==

Go to Plugins > Add New and search for "Export Comment Emails". Then install and activate it.

To use the plugin, go to Tools > Export Comment Emails.

== Frequently Asked Questions ==

== Screenshots ==

== Changelog ==

= 1.0 =
* Initial release

== Upgrade Notice ==
