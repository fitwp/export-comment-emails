/*global inlineEditPost, jQuery */
jQuery( function ( $ ) {
	'use strict';

	$( '#from-date' ).datepicker( {dateFormat: 'dd-mm-yy'} );
	$( '#to-date' ).datepicker( {dateFormat: 'dd-mm-yy'} );

	$( '#date' ).on( 'change', function () {
		$( '#custom-date' ).toggle( 'custom' === $( this ).val() )
	} ).trigger( 'change' );
} );
